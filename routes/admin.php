<?php

/** @var Router $router */
use Laravel\Lumen\Routing\Router;

use App\Models\Role;

$router->group(['middleware' => 'auth', 'prefix' => 'api'], function () use ($router) {
    $router->group(["middleware" => "roles:" . Role::ADMIN, "prefix" => "admin"], function () use ($router) {
        $router->get('users/{page}', 'UserController@index');

        $router->post('user/ban', 'UserController@ban');
        $router->post('user/unBan', 'UserController@unBan');

        $router->get('flaggedEntries/{page}', 'FlaggedEntryController@index');
        $router->get('flaggedComments/{page}', 'FlaggedCommentController@index');

        $router->post('entry/delete', 'EntryController@delete');
        $router->post('comment/delete', 'CommentController@adminDelete');

        $router->post('entry/dismiss', 'FlaggedEntryController@dismiss');
        $router->post('comment/dismiss', 'FlaggedCommentController@dismiss');

        $router->post('entry/report', 'FlaggedEntryController@report');

        $router->delete('comment/{commentId}', 'CommentController@delete');

        $router->delete('entry/{entryId}', 'EntryController@ownerDelete');
    });
});
