<?php

/** @var Router $router */
use Laravel\Lumen\Routing\Router;

$router->get("/", function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/entries/{page}', 'EntryController@list');
    $router->get('/entry/{id}', 'EntryController@show');

    $router->post('/search/entries/{page}', 'EntryController@search');

    $router->get('/comments/{entryId}/{page}', 'CommentController@list');

    $router->get('/categories', 'CategoryController@list');

    $router->post('/login', 'AuthController@login');
    $router->post('/register', 'AuthController@register');

    $router->post('/activateAccount/{token}', 'AuthController@activateAccount');

    $router->get('/getEntryImageUrl/{fileName}', "PictureController@getImageUrl");
});
