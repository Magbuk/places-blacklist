<?php

/** @var Router $router */
use Laravel\Lumen\Routing\Router;

$router->group(['middleware' => 'auth', 'prefix' => 'api'], function() use ($router){
    $router->post('logout', 'AuthController@logout');

    $router->post('token/refresh', 'AuthController@refresh');

    $router->get('me', 'AuthController@me');

    $router->post('entry', 'EntryController@create');
    $router->post('entry/{entryId}/picture', 'PictureController@createPicture');
    $router->delete('entry/{entryId}', 'EntryController@ownerDelete');

    $router->post('comment/{entryId}', 'CommentController@create');
    $router->delete('comment/{commentId}', 'CommentController@ownerDelete');

    $router->post('like/entry/{entryId}', 'LikeController@likeEntry');
    $router->post('dislike/entry/{entryId}', 'LikeController@dislikeEntry');

    $router->post('like/comment/{commentId}', 'LikeController@likeComment');
    $router->post('dislike/comment/{commentId}', 'LikeController@dislikeComment');

    $router->post('flag/entry/{entryId}', 'FlaggedEntryController@flagEntry');
    $router->post('flag/comment/{commentId}', 'FlaggedCommentController@flagComment');

    $router->post('picture/validate', 'PictureController@validatePicture');
});
