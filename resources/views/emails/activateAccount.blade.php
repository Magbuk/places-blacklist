<div>
    <div class="mail-content">
        <br>

        <p>Dziękujemy rejestrację. Twoje konto w Ostrzeżenniku jest już prawie gotowe.</p>
        <p>Kliknij w przycisk i dokonaj aktywacji, aby móc w pełni korzystać z portalu:
            <br></p>

        <a href={{ frontend_url()."/activate_account/" . $token }}>
            <div class="activation-button">
                Aktywacja konta
            </div>
        </a>

        <br><br><br><br>
        <p>Jeżeli przycisk nie działa, skopiuj poniższy adres i wklej w pole adresowe przeglądraki:</p>
        <p class="url">
            {{ frontend_url()."/activate_account/" . $token }}
        </p>
        <br><br>
        <p>Pozdrawiamy, </p>
        <p>Zespół Ostrzeżennik</p>
    </div>
</div>
