<div>
    <div class="mail-content">
        <br>
        <p>Otrzymaliśmy zgłoszenie od użytkownika portalu, które wydaje się niepokojące.</p>
        <p>Poniżej znajduje się jego treść.</p>
        <br><br><br>
        <p>Tytuł zgłoszenia: {{ $title }} </p>
        <p>Opis zgłoszenia: {{ $description }} </p>
        <p>Data wizyty: {{ $visit_date }} </p>

        <p>Zgłoszenie dodano: {{ $created_at }} </p>
        <br><br>
        <p>Pozdrawiamy, </p>
        <p>Zespół Ostrzeżennik</p>
    </div>
</div>
