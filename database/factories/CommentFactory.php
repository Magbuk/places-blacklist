<?php

use App\Models\Comment;

/* @var $factory Closure */
$factory->define(Comment::class, function (Faker\Generator $faker) {

    return [
        "user_id" => rand(1, DatabaseSeeder::USER_NUMBER),
        "entry_id" => rand(1, DatabaseSeeder::ENTRY_NUMBER),
        "content" => $faker->text($maxNbChars = 250),
        "author_deleted" => rand(0, 10) === 0 ? true : false,
        "mod_deleted" => rand(0, 10) === 0 ? true : false,
    ];
});
