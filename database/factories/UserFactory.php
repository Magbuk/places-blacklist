<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

/* @var $factory Closure */
$factory->define(User::class, function (Faker\Generator $faker) {

    return [
        "username" => $faker->unique()->firstName(),
        "email" => $faker->unique()->safeEmail(),
        "password" => Hash::make("password")
    ];
});
