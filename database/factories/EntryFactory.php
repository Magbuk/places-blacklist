<?php

use App\Models\Entry;

/* @var $factory Closure */
$factory->define(Entry::class, function (Faker\Generator $faker) {

    return [
        "user_id" => rand(1, DatabaseSeeder::USER_NUMBER),
        "title" => $faker->sentence($nbWords = 4, $variableNbWords = true),
        "description" => $faker->text($maxNbChars = 250),
        "address" => $faker->streetAddress(),
        "visit_date" => $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now'),
        "category_id" => rand(1, count(CategoriesTableSeeder::CATEGORIES)),
    ];
});
