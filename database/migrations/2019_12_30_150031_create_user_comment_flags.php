<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCommentFlags extends Migration
{
    public function up()
    {
        Schema::create('user_comment_flags', function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("comment_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->timestamps();

            $table->foreign("comment_id")->references("id")->on("comments")->onDelete("cascade");
            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('users_comment_flags');
    }
}
