<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntryCategoriesTable extends Migration
{
    public function up(): void
    {
        Schema::create("entry_categories", function (Blueprint $table): void {
            $table->increments("id");
            $table->string("name");
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("entry_categories");
    }
}
