<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserActivationsTable extends Migration
{
    public function up(): void
    {
        Schema::create("user_activations", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->boolean("activated")->default(false);
            $table->string("token")->nullable();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("user_activations");
    }
}
