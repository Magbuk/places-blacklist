<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    public function up(): void
    {
        Schema::create("user_roles", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->integer("role_id")->unsigned()->default(3);
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("role_id")->references("id")->on("roles")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("user_roles");
    }
}
