<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEntryLocation extends Migration
{
    public function up(): void
    {
        Schema::table("entries", function (Blueprint $table): void {
            $table->string("location_x")->default(null)->nullable();
            $table->string("location_y")->default(null)->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('entries', function (Blueprint $table): void {
            $table->dropColumn('location_x');
            $table->dropColumn('location_y');
        });
    }
}
