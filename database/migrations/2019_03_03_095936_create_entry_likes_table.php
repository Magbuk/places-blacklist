<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntryLikesTable extends Migration
{
    public function up(): void
    {
        Schema::create("entry_likes", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->integer("entry_id")->unsigned();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("entry_id")->references("id")->on("entries")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("entry_likes");
    }
}
