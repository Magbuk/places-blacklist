<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlaggedEntriesTable extends Migration
{
    public function up(): void
    {
        Schema::create("flagged_entries", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("entry_id")->unsigned();
            $table->boolean("dismissed")->default(false);
            $table->integer("reports_number")->default(0);
            $table->timestamps();

            $table->foreign("entry_id")->references("id")->on("entries")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("flagged_entries");
    }
}
