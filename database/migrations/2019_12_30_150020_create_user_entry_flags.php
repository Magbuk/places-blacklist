<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEntryFlags extends Migration
{
    public function up(): void
    {
        Schema::create("user_entry_flags", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("entry_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->timestamps();

            $table->foreign("entry_id")->references("id")->on("entries")->onDelete("cascade");
            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('users_entry_flags');
    }
}
