<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentDislikesTable extends Migration
{
    public function up(): void
    {
        Schema::create("comment_dislikes", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->integer("comment_id")->unsigned();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("comment_id")->references("id")->on("comments")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("comment_dislikes");
    }
}
