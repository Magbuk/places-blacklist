<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    public function up(): void
    {
        Schema::create("entries", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->string("title");
            $table->string("picture")->nullable();
            $table->text("description");
            $table->string("address");
            $table->integer("category_id")->unsigned();
            $table->string("visit_date")->nullable();
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("category_id")->references("id")->on("entry_categories")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("entries");
    }
}
