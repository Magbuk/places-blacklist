<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    public function up(): void
    {
        Schema::create("comments", function (Blueprint $table): void {
            $table->increments("id");
            $table->integer("user_id")->unsigned();
            $table->integer("entry_id")->unsigned();
            $table->string("content");
            $table->boolean("author_deleted")->default(false);
            $table->boolean("mod_deleted")->default(false);
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("entry_id")->references("id")->on("entries")->onDelete("cascade");
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("comments");
    }
}
