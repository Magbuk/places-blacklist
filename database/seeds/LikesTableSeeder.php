<?php

use App\Models\Comment;
use App\Models\CommentDislike;
use App\Models\CommentLike;
use App\Models\Entry;
use App\Models\EntryDislike;
use App\Models\EntryLike;
use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class LikesTableSeeder
 */
class LikesTableSeeder extends Seeder
{
    private const LIKE_INDEX = 1;
    private const DISLIKE_INDEX = 2;

    public function run(): void
    {
        $users = User::all();

        foreach ($users as $user) {
            $this->createEntryLikes($user);
            $this->createCommentLikes($user);
        }
    }

    /**
     * @param User $user
     */
    private function createEntryLikes(User $user): void
    {
        $entries = Entry::all();

        foreach ($entries as $entry) {
            $random = rand(0, 2);
            if ($random === static::LIKE_INDEX) {
                EntryLike::create([
                    "user_id" => $user->id,
                    "entry_id" => $entry->id,
                ]);
            } elseif ($random === static::DISLIKE_INDEX) {
                EntryDislike::create([
                    "user_id" => $user->id,
                    "entry_id" => $entry->id,
                ]);
            }
        }
    }

    /**
     * @param User $user
     */
    private function createCommentLikes(User $user): void
    {
        $comments = Comment::all();

        foreach ($comments as $comment) {
            $random = rand(0, 2);
            if ($random === static::LIKE_INDEX) {
                CommentLike::create([
                    "user_id" => $user->id,
                    "comment_id" => $comment->id,
                ]);
            } elseif ($random === static::DISLIKE_INDEX) {
                CommentDislike::create([
                    "user_id" => $user->id,
                    "comment_id" => $comment->id,
                ]);
            }
        }
    }
}
