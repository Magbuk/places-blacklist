<?php

use App\Models\EntryCategory;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public const CATEGORIES = [
        "Restauracja", "Dworzec", "Park", "Klub", "Inne"
    ];

    public function run(): void
    {
        foreach (static::CATEGORIES as $category) {
            EntryCategory::firstOrCreate([
                "name" => $category,
            ]);
        }
    }
}
