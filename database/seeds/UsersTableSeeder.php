<?php

use App\Models\User;
use App\Models\UserActivation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    public function run(): void
    {
        Mail::fake();

        $user = User::create([
            "email" => "admin@example.com",
            "username" => "admin",
            "password" => Hash::make("password"),
        ]);

        $this->activateUser($user);

        factory(User::class, DatabaseSeeder::USER_NUMBER)->create();
    }

    public function activateUser(User $user): void
    {
        $activation = UserActivation::where("user_id", $user->id)->firstOrFail();

        $activation->activated = true;

        $activation->save();
    }
}
