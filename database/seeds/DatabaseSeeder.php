<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    public const USER_NUMBER = 50;
    public const ENTRY_NUMBER = 100;
    public const COMMENT_NUMBER = 200;

    public function run(): void
    {
        $this->call('UsersTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('UserRolesTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('EntriesTableSeeder');
        $this->call('CommentsTableSeeder');
        $this->call('LikesTableSeeder');
    }
}
