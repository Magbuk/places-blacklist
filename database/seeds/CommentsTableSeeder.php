<?php

use App\Models\Comment;
use Illuminate\Database\Seeder;

/**
 * Class CommentsTableSeeder
 */
class CommentsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Comment::class, DatabaseSeeder::COMMENT_NUMBER)->create();
    }
}
