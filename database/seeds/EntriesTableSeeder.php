<?php

use App\Models\Entry;
use Illuminate\Database\Seeder;

/**
 * Class EntriesTableSeeder
 */
class EntriesTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Entry::class, DatabaseSeeder::ENTRY_NUMBER)->create();
    }
}
