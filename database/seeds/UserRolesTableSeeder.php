<?php

use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Database\Seeder;

/**
 * Class UserRolesTableSeeder
 */
class UserRolesTableSeeder extends Seeder
{
    public function run(): void
    {
        $users = User::all();

        foreach ($users as $user) {
            $role = new UserRole();

            $role->user_id = $user->id;
            $role->role_id = $user->id === 1 ? Role::SUPERADMIN : Role::USER;

            $role->save();
        }
    }
}
