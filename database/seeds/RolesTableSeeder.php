<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

/**
 * Class RolesTableSeeder
 */
class RolesTableSeeder extends Seeder
{
    public function run(): void
    {
        $roles = [
            Role::SUPERADMIN => "SUPERADMIN",
            Role::ADMIN => "ADMIN",
            Role::USER => "USER",
        ];

        foreach ($roles as $key => $value) {
            Role::firstOrCreate([
                "id" => $key,
                "name" => $value,
            ]);
        }
    }
}
