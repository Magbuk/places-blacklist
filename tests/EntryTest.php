<?php

declare(strict_types=1);

namespace Tests;

use App\Models\Entry;
use App\Models\EntryCategory;
use App\Models\User;
use Carbon\Carbon;
use Tests\Helpers\ObjectCreator;

/**
 * Class EntryTest
 * @package Tests
 */
class EntryTest extends TestCase
{
    use ObjectCreator;

    /** @var int */
    const FIRST_PAGE = 0;

    /** @test */
    public function listEntries(): void
    {
        $entry = $this->createEntry();

        $response = $this->json("GET", "api/entries/" . self::FIRST_PAGE)
            ->seeJson([
                "id" => $entry->id,
                "title" => $entry->title,
                "description" => $entry->description,
                "address" => $entry->address,
                "visitDate" => $entry->visit_date,
            ]);
        $response->assertResponseStatus(200);
    }

    /** @test */
    public function showEntry(): void
    {
        $entry = $this->createEntry();

        $response = $this->json("GET", "api/entry/" . $entry->id)
            ->seeJson([
                "id" => $entry->id,
                "title" => $entry->title,
                "description" => $entry->description,
                "address" => $entry->address,
                "visitDate" => $entry->visit_date,
            ]);
        $response->assertResponseStatus(200);
    }

    /** @test */
    public function addEntry(): void
    {
        $this->createMockUser();
        $category = $this->createCategory();

        $response = $this->json("POST", "/api/entry",
            [
                "title" => "Test",
                "description" => "Test description",
                "categoryId" => $category->id,
                "address" => "Test",
                "visitDate" => Carbon::now()->format("Y/m/d")
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Successfully added an entry", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function reportEntry(): void
    {
        $this->createMockUser();
        $entry = $this->createEntry();

        $response = $this->json("POST", "/api/flag/entry/" . $entry->id,
            [
                "entryId" => $entry->id,
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Successfully reported the entry", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function likeEntry(): void
    {
        $this->createMockUser();
        $entry = $this->createEntry();

        $response = $this->json("POST", "/api/like/entry/" . $entry->id,
            [
                "entryId" => $entry->id,
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Succesfully liked the entry", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function dislikeEntry(): void
    {
        $this->createMockUser();
        $entry = $this->createEntry();

        $response = $this->json("POST", "/api/dislike/entry/" . $entry->id,
            [
                "entryId" => $entry->id,
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Succesfully disliked the entry", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function deleteEntry(): void
    {
        $user = $this->createMockUser();
        $entry = $this->createEntry($user);

        $response = $this->json("DELETE", "/api/entry/" . $entry->id);
        $response->assertResponseStatus(200);
        $this->assertEquals("Successfully deleted the entry", json_decode($response->response->getContent())->message);
    }
}
