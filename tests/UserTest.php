<?php

declare(strict_types=1);

namespace Tests;

use App\Models\User;
use App\Models\UserActivation;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserTest
 * @package Tests
 */
class UserTest extends TestCase
{
    /** @test */
    public function unauthorizedProfileGetRequestFails(): void
    {
        $response = $this->json("GET", "/api/me");
        $response->assertResponseStatus(401);
    }

    /** @test */
    public function getRequestReturnsProfile(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->json("GET", "api/me")
            ->seeJson([
                "id" => $user->id,
                "username" => $user->username,
                "email" => $user->email,
            ]);
        $response->assertResponseStatus(200);
    }

    /** @test */
    public function login(): void
    {
        $user = $this->createTestUser();
        $this->activateAccount($user);
        $response = $this->json("POST", "/api/login",
            [
                "email" => $user->email,
                "password" => "password"
            ]);
        $response->assertResponseStatus(200);
    }

    /** @test */
    public function loginNotActivatedAccount(): void
    {
        $user = $this->createTestUser();

        $response = $this->json("POST", "api/login",
            [
                "email" => $user->email,
                "password" => "password"
            ]);
        $response->assertResponseStatus(401);
        $this->assertEquals("Account not activated", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function register(): void
    {
        $response = $this->json("POST", "/api/register",
            [
                "email" => "example@example.com",
                "username" => "Test",
                "password" => "password",
                "confirmPassword" => "password",
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Successfully registered an account", json_decode($response->response->getContent())->message);
    }

    /**
     * @return User
     */
    private function createTestUser(): User
    {
        $user = new User();
        $user->username = "Test";
        $user->email = "example@example.com";
        $user->password = Hash::make("password");
        $user->save();

        return $user;
    }

    /**
     * @param User $user
     */
    private function activateAccount(User $user): void
    {
        $activation = UserActivation::where("user_id", $user->id)->firstOrFail();
        $activation->activated = true;

        $activation->save();
    }
}
