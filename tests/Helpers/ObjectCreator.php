<?php

declare(strict_types=1);

namespace Tests\Helpers;

use App\Models\Comment;
use App\Models\Entry;
use App\Models\EntryCategory;
use App\Models\User;
use Carbon\Carbon;

/**
 * Trait ObjectCreator
 * @package Tests
 */
trait ObjectCreator
{
    /**
     * @return User
     */
    private function createMockUser(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        return $user;
    }

    /**
     * @return EntryCategory
     */
    protected function createCategory(): EntryCategory
    {
        $category = new EntryCategory();
        $category->name = "Test";
        $category->save();

        return $category;
    }

    /**
     * @param User $existingUser
     * @return Entry
     */
    protected function createEntry(User $existingUser = null): Entry
    {
        $category = $this->createCategory();

        $user = $existingUser ? $existingUser : factory(User::class)->create();

        $entry = new Entry();
        $entry->user_id = $user->id;
        $entry->title = "Test";
        $entry->description = "Test description";
        $entry->category_id = $category->id;
        $entry->address = "Test";
        $entry->visit_date = Carbon::now()->format("Y/m/d");
        $entry->save();

        return $entry;
    }

    /**
     * @param Entry $entry
     * @param User|null $existingUser
     * @return Comment
     */
    protected function createComment(Entry $entry, User $existingUser = null): Comment
    {
        $user = $existingUser ? $existingUser : factory(User::class)->create();

        $comment = new Comment();
        $comment->user_id = $user->id;
        $comment->entry_id = $entry->id;
        $comment->content = "Test";
        $comment->save();

        return $comment;
    }
}
