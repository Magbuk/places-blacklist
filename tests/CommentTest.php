<?php

declare(strict_types=1);

namespace Tests;

use Tests\Helpers\ObjectCreator;

/**
 * Class CommentTest
 * @package Tests
 */
class CommentTest extends TestCase
{
    use ObjectCreator;

    /** @var int */
    const FIRST_PAGE = 0;

    /** @test */
    public function listComment(): void
    {
        $entry = $this->createEntry();
        $comment = $this->createComment($entry);

        $response = $this->json("GET", "api/comments/" . $entry->id . "/". self::FIRST_PAGE)
            ->seeJson([
                "id" => $comment->id,
                "content" => $comment->content,
            ]);
        $response->assertResponseStatus(200);
    }

    /** @test */
    public function addComment(): void
    {
        $this->createMockUser();
        $entry = $this->createEntry();

        $response = $this->json("POST", "/api/comment/" . $entry->id,
            [
                "content" => "Test",
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Successfully added a comment", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function reportComment(): void
    {
        $this->createMockUser();
        $entry = $this->createEntry();
        $comment = $this->createComment($entry);

        $response = $this->json("POST", "/api/flag/comment/" . $comment->id,
            [
                "commentId" => $comment->id,
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Successfully reported the comment", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function likeEntry(): void
    {
        $this->createMockUser();
        $entry = $this->createEntry();
        $comment = $this->createComment($entry);

        $response = $this->json("POST", "/api/like/comment/" . $comment->id,
            [
                "commentId" => $comment->id,
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Succesfully liked the comment", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function dislikeComment(): void
    {
        $this->createMockUser();
        $entry = $this->createEntry();
        $comment = $this->createComment($entry);

        $response = $this->json("POST", "/api/dislike/comment/" . $comment->id,
            [
                "commentId" => $comment->id,
            ]);
        $response->assertResponseStatus(200);
        $this->assertEquals("Succesfully disliked the comment", json_decode($response->response->getContent())->message);
    }

    /** @test */
    public function deleteComment(): void
    {
        $user = $this->createMockUser();
        $entry = $this->createEntry($user);
        $comment = $this->createComment($entry, $user);

        $response = $this->json("DELETE", "/api/comment/" . $comment->id);
        $response->assertResponseStatus(200);
        $this->assertEquals("Successfully deleted the comment", json_decode($response->response->getContent())->message);
    }
}
