<?php
if (!function_exists('config_path')) {
    function config_path($path = '')
    {
        if ($path === "profanity.php") {
            return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
        }
    }

    if (!function_exists('frontend_url')) {
        function frontend_url()
        {
            return getenv('FRONTEND_URL');
        }
    }

    if (!function_exists('security_service')) {
        function security_service()
        {
            return getenv('SECURITY_SERVICE_MAIL');
        }
    }
}
