<?php

namespace App\Helpers;

use App\Models\CommentDislike;
use App\Models\CommentLike;
use App\Models\EntryDislike;
use App\Models\EntryLike;
use Illuminate\Support\Facades\Auth;

/**
 * Class IsLiked
 * @package App\Helpers
 */
class IsLiked
{
    /**
     * @param int $commentId
     * @return bool
     */
    public static function isCommentLiked(int $commentId): bool
    {
        return Auth::user() ? !!CommentLike::where('user_id', Auth::user()->id)->where('comment_id', $commentId)->first() : false;
    }

    /**
     * @param int $commentId
     * @return bool
     */
    public static function isCommentDisliked(int $commentId): bool
    {
        return Auth::user() ? !!CommentDislike::where('user_id', Auth::user()->id)->where('comment_id', $commentId)->first() : false;
    }

    /**
     * @param int $entryId
     * @return bool
     */
    public static function isEntryLiked(int $entryId): bool
    {
        return Auth::user() ? !!EntryLike::where('user_id', Auth::user()->id)->where('entry_id', $entryId)->first() : false;
    }

    /**
     * @param int $entryId
     * @return bool
     */
    public static function isEntryDisliked(int $entryId): bool
    {
        return Auth::user() ? !!EntryDislike::where('user_id', Auth::user()->id)->where('entry_id', $entryId)->first() : false;
    }
}
