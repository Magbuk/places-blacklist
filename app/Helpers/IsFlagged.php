<?php

namespace App\Helpers;

use App\Models\CommentLike;
use App\Models\EntryLike;
use App\Models\UserCommentFlag;
use App\Models\UserEntryFlag;
use Illuminate\Support\Facades\Auth;

/**
 * Class IsFlagged
 * @package App\Helpers
 */
class IsFlagged
{
    /**
     * @param int $commentId
     * @return bool
     */
    public static function isCommentFlagged(int $commentId): bool
    {
        return Auth::user() ? !!UserCommentFlag::where('user_id', Auth::user()->id)->where('comment_id', $commentId)->first() : false;
    }

    /**
     * @param int $entryId
     * @return bool
     */
    public static function isEntryFlagged(int $entryId): bool
    {
        return Auth::user() ? !!UserEntryFlag::where('user_id', Auth::user()->id)->where('entry_id', $entryId)->first() : false;
    }
}
