<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Comment;
use App\Models\Entry;
use App\Models\FlaggedComment;
use App\Models\FlaggedEntry;
use App\Models\UserCommentFlag;
use App\Models\UserEntryFlag;

/**
 * Class FlagService
 * @package App\Services
 */
class FlagService
{
    /**
     * @param string $entryId
     * @param int $userId
     */
    public function flagEntry(string $entryId, int $userId): void
    {
        $entry = Entry::findOrFail($entryId);

        if ($entry && !$entryFlag = UserEntryFlag::where("user_id", $userId)->where("entry_id", $entryId)->first()) {
            $entryFlag = new UserEntryFlag();
            $entryFlag->user_id = $userId;
            $entryFlag->entry_id = $entryId;
            $entryFlag->save();

            $flaggedRecord = FlaggedEntry::where("entry_id", $entryId)->firstOrFail();
            $flaggedRecord->reports_number = $flaggedRecord->reports_number + 1;
            $flaggedRecord->save();
        }
    }

    /**
     * @param string $commentId
     * @param int $userId
     */
    public function flagComment(string $commentId, int $userId): void
    {
        $comment = Comment::findOrFail($commentId);

        if ($comment && !$commentFlag = UserCommentFlag::where("user_id", $userId)->where("comment_id", $commentId)->first()) {
            $commentFlag = new UserCommentFlag();
            $commentFlag->user_id = $userId;
            $commentFlag->comment_id = $commentId;
            $commentFlag->save();

            $flaggedRecord = FlaggedComment::where("comment_id", $commentId)->firstOrFail();
            $flaggedRecord->reports_number = $flaggedRecord->reports_number + 1;
            $flaggedRecord->save();
        }
    }
}
