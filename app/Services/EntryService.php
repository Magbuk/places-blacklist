<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ApiException;
use App\Http\Requests\ManageEntry;
use App\Http\Requests\SearchEntry;
use App\Models\Entry;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Class EntryService
 * @package App\Services
 */
class EntryService
{
    private const ENTRIES_SHOWN = 20;

    /**
     * @param string $page
     * @return Collection
     */
    public function list(string $page): Collection
    {
        $skipEntries = $page * static::ENTRIES_SHOWN;
        if ($skipEntries > count(Entry::all()) || $skipEntries > 200) {
            return collect([]);
        }

        return Entry::all()->sortByDesc("created_at")
            ->slice($skipEntries)
            ->take(static::ENTRIES_SHOWN);
    }

    /**
     * @param SearchEntry $search
     * @param string $page
     * @return Collection
     */
    public function search(SearchEntry $search, string $page): Collection
    {
        $query = $this->getQuery($search);

        $skipEntries = $page * static::ENTRIES_SHOWN;
        if ($skipEntries > count($query->get())) {
            return collect([]);
        }

        return $query->get()
            ->sortByDesc("created_at")
            ->slice($skipEntries)
            ->take(static::ENTRIES_SHOWN);
    }

    /**
     * @param array $data
     * @param int $userId
     * @return Entry
     */
    public function createEntry(array $data, int $userId): Entry
    {
        $entry = new Entry();

        $entry->user_id = $userId;
        $entry->title = $data["title"];
        $entry->picture = $data["picture"] ?? "";
        $entry->description = app('profanityFilter')->replaceWith('*')->filter($data["description"]);
        $entry->address = $data["address"];
        $entry->category_id = $data["categoryId"];
        $entry->visit_date = $data["visitDate"];

        if (isset($data["location"])) {
            $entry->location_x = (string)$data["location"]["x"];
            $entry->location_y = (string)$data["location"]["y"];
        }

        $entry->save();

        return $entry;
    }

    /**
     * @param UploadedFile $picture
     * @param int $entryId
     * @param int $userId
     */
    public function createEntryPicture(UploadedFile $picture, int $entryId, int $userId): void
    {
        $entry = Entry::findOrFail($entryId);

        if ($entry->user_id !== $userId) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(["Cannot add picture"]);
        }

        $extension = $picture->getClientOriginalExtension();
        $fileName = "entry_" . $entryId . "_image";
        $filePath = $fileName . "" . "" . $extension;

        Storage::putFileAs("entry", $picture, $filePath);

        $entry->picture = $filePath;

        $entry->save();
    }

    /**
     * @param string $fileName
     * @return string
     */
    public function getImageUrl(string $fileName = ""): string
    {
        if ($fileName) {
            return storage_path("app/entry/" . $fileName);
        }

        return "";
    }

    /**
     * @param ManageEntry $entryId
     * @throws Exception
     */
    public function deleteEntry(ManageEntry $entryId): void
    {
        $entry = Entry::findOrFail($entryId)->first();

        $entry->delete();
    }

    /**
     * @param int $entryId
     * @param int $userId
     * @throws Exception
     */
    public function ownerDeleteEntry(int $entryId, int $userId): void
    {
        $entry = Entry::findOrFail($entryId);

        if ($userId && $entry->user_id !== $userId) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(["Cannot delete the entry"]);
        }

        $entry->delete();
    }

    /**
     * @param SearchEntry $search
     * @return Builder
     */
    protected function getQuery(SearchEntry $search): Builder
    {
        $query = Entry::query();

        if ($search["username"]) {
            $user = User::where("username", $search["username"])->first();

            if ($user) {
                $query = $query->where('user_id', $user->id);
            }
        }

        if ($search["address"]) {
            $query = $query->where('address', 'like', '%' . $search["address"] . '%');
        }

        if ($search["categoryId"]) {
            $query = $query->where('category_id', $search["categoryId"]);
        }

        return $query;
    }
}
