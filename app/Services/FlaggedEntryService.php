<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ApiException;
use App\Http\Requests\ManageReport;
use App\Models\FlaggedEntry;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

/**
 * Class FlaggedEntryService
 * @package App\Services
 */
class FlaggedEntryService
{
    private const FLAGGED_ENTRIES_SHOWN = 25;

    /**
     * @param string $page
     * @return Collection
     */
    public function indexFlagged(string $page): Collection
    {
        $totalPages = $this->getPagesCount();
        $skipEntries = $page * static::FLAGGED_ENTRIES_SHOWN;

        if ($page > $totalPages) {
            return collect([]);
        } else {
            return FlaggedEntry::all()->where("dismissed", false)
                ->where("reports_number",">", 0)
                ->sortByDesc("reports_number")
                ->slice($skipEntries)
                ->take(static::FLAGGED_ENTRIES_SHOWN);
        }
    }

    /**
     * @return int
     */
    public function getPagesCount(): int
    {
        $entriesCount = count(FlaggedEntry::all()->where("dismissed", false)
            ->where("reports_number",">", 0));

        return (int)ceil($entriesCount / static::FLAGGED_ENTRIES_SHOWN);
    }

    /**
     * @param ManageReport $reportId
     */
    public function dismiss(ManageReport $reportId): void
    {
        $flaggedEntry = FlaggedEntry::findOrFail($reportId)->first();

        if ($flaggedEntry->dismissed) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['Report was already dismissed']);
        } else {
            $flaggedEntry->dismissed = true;

            $flaggedEntry->save();
        }
    }

    /**
     * @param ManageReport $reportId
     */
    public function report(ManageReport $reportId): void
    {
        $flaggedEntry = FlaggedEntry::findOrFail($reportId)->first();
        $entry = $flaggedEntry->entry;

        $data = [
            "title" => $entry->title,
            "description" => $entry->description,
            "created_at" => $entry->created_at,
            "visit_date" => $entry->visit_date,
        ];

        Mail::send("emails.reportEntry", $data, function ($m) {
            $m->to(getenv('SECURITY_SERVICE_MAIL'))->subject('Zgłoszenie z portalu Ostrzezennik');
        });
    }
}
