<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ApiException;
use App\Http\Requests\ManageReport;
use App\Models\FlaggedComment;
use App\Models\FlaggedEntry;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

/**
 * Class FlaggedCommentService
 * @package App\Services
 */
class FlaggedCommentService
{
    private const FLAGGED_COMMENTS_SHOWN = 25;

    /**
     * @param string $page
     * @return Collection
     */
    public function indexFlagged(string $page): Collection
    {
        $totalPages = $this->getPagesCount();
        $skipComments = $page * static::FLAGGED_COMMENTS_SHOWN;

        if ($page > $totalPages) {
            return collect([]);
        } else {
            return FlaggedComment::all()->where("dismissed", false)
                ->where("reports_number", ">", 0)
                ->sortByDesc("reports_number")
                ->slice($skipComments)
                ->take(static::FLAGGED_COMMENTS_SHOWN);
        }
    }

    /**
     * @return int
     */
    public function getPagesCount(): int
    {
        $commentsCount = count(FlaggedComment::all()->where("dismissed", false)
            ->where("reports_number", ">", 0));

        return (int)ceil($commentsCount / static::FLAGGED_COMMENTS_SHOWN);
    }

    /**
     * @param ManageReport $reportId
     */
    public function dismiss(ManageReport $reportId): void
    {
        $flaggedComment = FlaggedComment::findOrFail($reportId)->first();

        if ($flaggedComment->dismissed) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['Report was already dismissed']);
        } else {
            $flaggedComment->dismissed = true;

            $flaggedComment->save();
        }
    }
}
