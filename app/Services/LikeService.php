<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\Comment;
use App\Models\CommentDislike;
use App\Models\CommentLike;
use App\Models\Entry;
use App\Models\EntryDislike;
use App\Models\EntryLike;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * Class LikeService
 * @package App\Services
 */
class LikeService
{
    /**
     * @param string $entryId
     * @param int $userId
     * @return mixed
     */
    public function likeEntry(string $entryId, int $userId): string
    {
        $entry = Entry::findOrFail($entryId);

        if ($like = EntryLike::where('user_id', $userId)->where('entry_id', $entry->id)->first()) {
            $like->delete();
            return "Succesfully unliked the entry";
        } else {
            if ($dislike = EntryDislike::where('user_id', $userId)->where('entry_id', $entry->id)->first()) {
                $dislike->delete();
            }

            $this->createEntryLike($entryId, $userId);
            return "Succesfully liked the entry";
        }
    }

    /**
     * @param string $entryId
     * @param int $userId
     * @return mixed
     */
    public function dislikeEntry(string $entryId, int $userId): string
    {
        $entry = Entry::findOrFail($entryId);

        if ($dislike = EntryDislike::where('user_id', $userId)->where('entry_id', $entry->id)->first()) {
            $dislike->delete();
            return "Succesfully undisliked the entry";
        } else {
            if ($like = EntryLike::where('user_id', $userId)->where('entry_id', $entry->id)->first()) {
                $like->delete();
            }

            $this->createEntryDislike($entryId, $userId);
            return "Succesfully disliked the entry";
        }
    }

    /**
     * @param string $commentId
     * @param int $userId
     * @return mixed
     */
    public function likeComment(string $commentId, int $userId): string
    {
        $comment = Comment::findOrFail($commentId);

        if ($like = CommentLike::where('user_id', $userId)->where('comment_id', $comment->id)->first()) {
            $like->delete();
            return "Succesfully unliked the comment";
        } else {
            if ($dislike = CommentDislike::where('user_id', $userId)->where('comment_id', $comment->id)->first()) {
                $dislike->delete();
            }

            $this->createCommentLike($commentId, $userId);
            return "Succesfully liked the comment";
        }
    }

    /**
     * @param string $commentId
     * @param int $userId
     * @return mixed
     */
    public function dislikeComment(string $commentId, int $userId): string
    {
        $comment = Comment::findOrFail($commentId);

        if ($dislike = CommentDislike::where('user_id', $userId)->where('comment_id', $comment->id)->first()) {
            $dislike->delete();
            return "Succesfully undisliked the comment";
        } else {
            if ($like = CommentLike::where('user_id', $userId)->where('comment_id', $comment->id)->first()) {
                $like->delete();
            }

            $this->createCommentDislike($commentId, $userId);
            return "Succesfully disliked the comment";
        }
    }

    /**
     * @param string $entryId
     * @param int $userId
     * @return mixed
     */
    protected function createEntryLike(string $entryId, int $userId): void
    {
        $like = new EntryLike();

        $like->user_id = $userId;
        $like->entry_id = $entryId;

        $like->save();
    }

    /**
     * @param string $entryId
     * @param int $userId
     * @return mixed
     */
    protected function createEntryDislike(string $entryId, int $userId): void
    {
        $dislike = new EntryDislike();

        $dislike->user_id = $userId;
        $dislike->entry_id = $entryId;

        $dislike->save();
    }

    /**
     * @param string $commentId
     * @param int $userId
     * @return mixed
     */
    protected function createCommentLike(string $commentId, int $userId): void
    {
        $like = new CommentLike();

        $like->user_id = $userId;
        $like->comment_id = $commentId;

        $like->save();
    }

    /**
     * @param string $commentId
     * @param int $userId
     * @return mixed
     */
    protected function createCommentDislike(string $commentId, int $userId): void
    {
        $dislike = new CommentDislike();

        $dislike->user_id = $userId;
        $dislike->comment_id = $commentId;

        $dislike->save();
    }
}
