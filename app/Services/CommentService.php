<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ApiException;
use App\Http\Requests\ManageComment;
use App\Models\Comment;
use App\Models\Entry;
use App\Models\FlaggedComment;
use Askedio\Laravel5ProfanityFilter\ProfanityFilter;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

/**
 * Class CommentService
 * @package App\Services
 */
class CommentService
{
    private const COMMENTS_SHOWN = 15;

    /**
     * @param string $entryId
     * @param string $page
     * @return Collection
     */
    public function list(string $entryId, string $page): Collection
    {
        $skipEntries = $page * static::COMMENTS_SHOWN;
        if ($skipEntries > count(Comment::all()->where('entry_id', $entryId))) {
            return collect([]);
        }

        return Comment::all()->where('entry_id', $entryId)
            ->sortByDesc("created_at")
            ->slice($skipEntries)
            ->take(static::COMMENTS_SHOWN);
    }

    /**
     * @param array $data
     * @param int $userId
     * @param string $entryId
     */
    public function createComment(array $data, int $userId, string $entryId)
    {
        if (!Entry::findOrFail($entryId)) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['Entry not found']);
        }

        $comment = new Comment();

        $comment->user_id = $userId;
        $comment->entry_id = $entryId;
        $comment->content = app('profanityFilter')->replaceWith('*')->filter($data["content"]);

        $comment->save();
    }

    /**
     * @param string $commentId
     * @param int $userId
     */
    public function ownerDeleteComment(string $commentId, int $userId)
    {
        $comment = Comment::findOrFail($commentId);

        if ($comment->user_id !== $userId) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['Cannot delete the comment']);
        }

        $this->checkIfDeleted($comment);
        $comment->author_deleted = true;

        $this->deleteReports($comment->id);

        $comment->save();
    }

    /**
     * @param ManageComment $commentId
     */
    public function adminDeleteComment(ManageComment $commentId)
    {
        $comment = Comment::findOrFail($commentId)->first();

        $this->checkIfDeleted($comment);
        $comment->mod_deleted = true;

        $this->deleteReports($comment->id);

        $comment->save();
    }

    /**
     * @param Comment $comment
     */
    private function checkIfDeleted(Comment $comment): void
    {
        if ($comment->author_deleted || $comment->mod_deleted) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['Comment not found']);
        }
    }

    /**
     * @param int $commentId
     */
    private function deleteReports(int $commentId): void
    {
        $commentReport = FlaggedComment::where("comment_id", $commentId)->firstOrFail();

        if ($commentReport) {
            $commentReport->delete();
        }
    }
}
