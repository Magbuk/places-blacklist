<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\Role;
use App\Models\User;
use App\Models\UserActivation;
use App\Models\UserRole;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthService
 * @package App\Services
 */
class AuthService
{
    /**
     * @param array $data
     */
    public function createUser(array $data): void
    {
        $user = new User();

        $user->email = $data['email'];
        $user->username = $data['username'];
        $user->password = Hash::make($data['password']);

        $user->save();
        $this->createUserRole($user);
    }

    /**
     * @param User $user
     */
    public function createUserRole(User $user): void
    {
        $userRole = new UserRole();

        $userRole->user_id = $user->id;
        $userRole->role_id = Role::USER;

        $userRole->save();
    }

    /**
     * @param string $token
     */
    public function activateAccount(string $token): void
    {
        $userActivation = UserActivation::where("token", $token)->firstOrFail();

        if (!$userActivation->activated) {
            $userActivation->activated = true;

            $userActivation->save();
        } else {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['Account already activated']);
        }
    }
}
