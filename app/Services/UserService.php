<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ApiException;
use App\Http\Requests\ManageUser;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    /** @var int */
    private const USERS_PER_PAGE = 25;

    /**
     * @param string $page
     * @return Collection
     */
    public function index(string $page): Collection
    {
        $totalPages = $this->getPagesCount();
        $skipEntries = static::USERS_PER_PAGE * $page;

        if ($page > $totalPages) {
            return collect([]);
        } else {
            return User::all()->sortBy('username')
                ->slice($skipEntries)
                ->take(static::USERS_PER_PAGE);
        }
    }

    /**
     * @return int
     */
    public function getPagesCount(): int
    {
        $userCount = count(User::all());

        return (int)ceil($userCount / static::USERS_PER_PAGE);
    }

    /**
     * @param ManageUser $userId
     * @return void
     */
    public function ban(ManageUser $userId): void
    {
        $user = User::findOrFail($userId)->first();

        if ($user->is_banned || $user->userRole->role_id === Role::SUPERADMIN) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['User cannot be banned']);
        } else {
            $user->is_banned = true;

            $user->save();
        }
    }

    /**
     * @param ManageUser $userId
     * @return void
     */
    public function unBan(ManageUser $userId): void
    {
        $user = User::findOrFail($userId)->first();

        if (!$user->is_banned) {
            throw (new ApiException())->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setMessages(['User cannot be banned']);
        } else {
            $user->is_banned = false;

            $user->save();
        }
    }
}
