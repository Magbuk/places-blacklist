<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\Entry;
use App\Models\FlaggedEntry;

/**
 * Class EntryObserver
 * @package App\Observers
 */
class EntryObserver
{
    /**
     * @param Entry $entry
     */
    public function created(Entry $entry): void
    {
        $flaggedEntry = new FlaggedEntry();

        $flaggedEntry->entry_id = $entry->id;

        $flaggedEntry->save();
    }
}
