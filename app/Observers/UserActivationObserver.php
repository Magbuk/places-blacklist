<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\User;
use App\Models\UserActivation;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserActivationObserver
 * @package App\Observers
 */
class UserActivationObserver
{
    /**
     * @param UserActivation $activation
     */
    public function created(UserActivation $activation): void
    {
        $data = [
            "token" => $activation->token,
        ];

        $user = User::findOrFail($activation->user_id);

        Mail::send("emails.activateAccount", $data, function ($m) use ($user) {
            $m->to($user->email)->subject('Rejestracja konta Ostrzezennik');
        });
    }
}
