<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\User;
use App\Models\UserActivation;

/**
 * Class UserObserver
 * @package App\Observers
 */
class UserObserver
{
    /**
     * @param User $user
     */
    public function created(User $user): void
    {
        $activation = new UserActivation();

        $activation->token = md5(uniqid($user->username, true));
        $activation->user_id = $user->id;

        $activation->save();
    }
}
