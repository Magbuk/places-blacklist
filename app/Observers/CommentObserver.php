<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\Comment;
use App\Models\FlaggedComment;

/**
 * Class CommentObserver
 * @package App\Observers
 */
class CommentObserver
{
    /**
     * @param Comment $comment
     */
    public function created(Comment $comment): void
    {
        $flaggedComment = new FlaggedComment();

        $flaggedComment->comment_id = $comment->id;

        $flaggedComment->save();
    }
}
