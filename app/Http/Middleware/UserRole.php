<?php

declare(strict_types=1);

namespace App\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  Closure $next
     * @param integer $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::user()->role()->role_id > (int)$role) {
            return response('Unauthorized.', 401);
        }
        return $next($request);
    }
}
