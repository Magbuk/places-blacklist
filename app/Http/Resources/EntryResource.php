<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Helpers\IsFlagged;
use App\Helpers\IsLiked;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class EntryResource
 * @package App\Http\Resources
 */
class EntryResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "picture" => $this->picture,
            "description" => $this->description,
            "address" => $this->address,
            "visitDate" => $this->visit_date,
            "category" => [
                "id" => $this->entryCategory["id"],
                "name" => $this->entryCategory["name"],
            ],
            "user" => [
                "id" => $this->user["id"],
                "username" => $this->user["username"],
                "isBanned" => $this->user["is_banned"],
            ],
            "createdAt" => $this->created_at,
            "likesCount" => count($this->likes),
            "dislikesCount" => count($this->dislikes),
            "selfLiked" => IsLiked::isEntryLiked($this->id),
            "selfDisliked" => IsLiked::isEntryDisliked($this->id),
            "selfFlagged" => IsFlagged::isEntryFlagged($this->id),
            "location" => [
                "x" => $this->location_x,
                "y" => $this->location_y,
            ]
        ];
    }
}
