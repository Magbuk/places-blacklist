<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FlaggedEntryResource
 * @package App\Http\Resources
 */
class FlaggedEntryResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "entry" => [
                "id" => $this->entry["id"],
                "title" => $this->entry["title"],
                "description" => $this->entry["description"],
                "address" => $this->entry["address"],
                "visitDate" => $this->entry["visit_date"],
                "user" => [
                    "id" => $this->entry["user"]["id"],
                    "username" => $this->entry["user"]["username"],
                ],
                "createdAt" => $this->entry["created_at"],
            ],
            "reportsNumber" => $this->reports_number,
        ];
    }
}
