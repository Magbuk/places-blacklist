<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FlaggedCommentResource
 * @package App\Http\Resources
 */
class FlaggedCommentResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "comment" => [
                "id" => $this->comment["id"],
                "content" => $this->comment["content"],
                "user" => [
                    "id" => $this->comment["user"]["id"],
                    "username" => $this->comment["user"]["username"],
                ],
                "createdAt" => $this->comment["created_at"],
            ],
            "reportsNumber" => $this->reports_number,
        ];
    }
}
