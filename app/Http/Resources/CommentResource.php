<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Helpers\IsFlagged;
use App\Helpers\IsLiked;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CommentResource
 * @package App\Http\Resources
 */
class CommentResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "content" => $this->content,
            "authorDeleted" => $this->author_deleted,
            "modDeleted" => $this->mod_deleted,
            "user" => [
                "id" => $this->user["id"],
                "username" => $this->user["username"],
                "isBanned" => $this->user["is_banned"],
            ],
            "createdAt" => $this->created_at,
            "likesCount" => count($this->likes),
            "dislikesCount" => count($this->dislikes),
            "selfLiked" => IsLiked::isCommentLiked($this->id),
            "selfDisliked" => IsLiked::isCommentDisliked($this->id),
            "selfFlagged" => IsFlagged::isCommentFlagged($this->id),
        ];
    }
}
