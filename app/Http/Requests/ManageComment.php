<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class ManageComment
 * @package App\Http\Requests
 */
class ManageComment extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "commentId" => "required|numeric"
        ];
    }
}
