<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class CreateComment
 * @package App\Http\Requests
 */
class CreateComment extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'content' => 'required|string|min:3|max:500',
        ];
    }
}
