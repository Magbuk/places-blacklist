<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class CreateEntryPicture
 * @package App\Http\Requests
 */
class CreateEntryPicture extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'picture' => 'nullable|image|mimes:jpeg,png,jpg,bmp|max:2000000',
        ];
    }
}
