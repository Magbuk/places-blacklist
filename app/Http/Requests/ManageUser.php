<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class ManageUser
 * @package App\Http\Requests
 */
class ManageUser extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "userId" => "required|numeric"
        ];
    }
}
