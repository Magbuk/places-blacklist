<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class SearchEntry
 * @package App\Http\Requests
 */
class SearchEntry extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "address" => "string",
            "username" => "string",
            "categoryId" => "int",
        ];
    }
}
