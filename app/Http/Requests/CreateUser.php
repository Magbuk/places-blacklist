<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class CreateUser
 * @package App\Http\Requests
 */
class CreateUser extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|unique:users',
            'username' => 'required|min:3|max:15|string|unique:users|regex:/^[A-Za-z0-9 ]+$/|profanity',
            'password' => 'required|min:6|max:50',
            'confirmPassword' => 'required|same:password',
        ];
    }
}
