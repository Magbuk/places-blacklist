<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class ManageEntry
 * @package App\Http\Requests
 */
class ManageEntry extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "entryId" => "required|numeric"
        ];
    }
}
