<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class CreateEntry
 * @package App\Http\Requests
 */
class CreateEntry extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|min:3|max:100|profanity',
            'description' => 'required|string|min:3|max:1000',
            'address' => 'required|string|profanity',
            'location' => 'array',
            'location.x' => 'numeric',
            'location.y' => 'numeric',
            'categoryId' => 'required|int',
            'visitDate' => 'required|date',
        ];
    }
}
