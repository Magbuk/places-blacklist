<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Anik\Form\FormRequest;

/**
 * Class ManageReport
 * @package App\Http\Requests
 */
class ManageReport extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "reportId" => "required|numeric"
        ];
    }
}
