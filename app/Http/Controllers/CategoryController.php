<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\EntryCategory;
use Illuminate\Http\JsonResponse;

/**
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        return response()->json(EntryCategory::all());
    }
}
