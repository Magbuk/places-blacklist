<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Collections\EntryCollection;
use App\Http\Requests\CreateEntry;
use App\Http\Requests\CreateEntryPicture;
use App\Http\Requests\ManageEntry;
use App\Http\Requests\SearchEntry;
use App\Http\Resources\EntryResource;
use App\Models\Entry;
use App\Services\EntryService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class EntryController
 * @package App\Http\Controllers
 */
class EntryController extends Controller
{
    /**
     * @var EntryService
     */
    private $entryService;

    /**
     * @param EntryService $entryService
     */
    public function __construct(EntryService $entryService)
    {
        $this->entryService = $entryService;
    }

    /**
     * @param string $page
     * @return EntryCollection
     */
    public function list(string $page): EntryCollection
    {
        return new EntryCollection($this->entryService->list($page));
    }

    /**
     * @param SearchEntry $query
     * @param string $page
     * @return EntryCollection
     */
    public function search(SearchEntry $query, string $page): EntryCollection
    {
        return new EntryCollection($this->entryService->search($query, $page));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return response()->json(new EntryResource(Entry::findOrFail($id)));
    }

    /**
     * @param CreateEntry $request
     * @return JsonResponse
     */
    public function create(CreateEntry $request): JsonResponse
    {
        $userId = Auth::user()->id;
        $entry = $this->entryService->createEntry($request->all(), $userId);

        return response()->json(['message' => 'Successfully added an entry',
            'entry' => $entry]);
    }

    /**
     * @param ManageEntry $entryId
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(ManageEntry $entryId): JsonResponse
    {
        $this->entryService->deleteEntry($entryId);

        return response()->json(['message' => 'Successfully deleted the entry']);
    }

    /**
     * @param int $entryId
     * @return JsonResponse
     * @throws Exception
     */
    public function ownerDelete(int $entryId): JsonResponse
    {
        $this->entryService->ownerDeleteEntry($entryId, Auth::user()->id);

        return response()->json(['message' => 'Successfully deleted the entry']);
    }
}
