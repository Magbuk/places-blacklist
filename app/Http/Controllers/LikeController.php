<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\LikeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class LikeController
 * @package App\Http\Controllers
 */
class LikeController extends Controller
{
    /**
     * @var LikeService
     */
    private $likeService;

    /**
     * @param LikeService $likeService
     */
    public function __construct(LikeService $likeService)
    {
        $this->likeService = $likeService;
    }

    /**
     * @param string $entryId
     * @return JsonResponse
     */
    public function likeEntry(string $entryId): JsonResponse
    {
        $message = $this->likeService->likeEntry($entryId, Auth::user()->id);

        return response()->json(['message' => $message]);
    }

    /**
     * @param string $entryId
     * @return JsonResponse
     */
    public function dislikeEntry(string $entryId): JsonResponse
    {
        $message = $this->likeService->dislikeEntry($entryId, Auth::user()->id);

        return response()->json(['message' => $message]);
    }

    /**
     * @param string $commentId
     * @return JsonResponse
     */
    public function likeComment(string $commentId): JsonResponse
    {
        $message = $this->likeService->likeComment($commentId, Auth::user()->id);

        return response()->json(['message' => $message]);
    }

    /**
     * @param string $commentId
     * @return JsonResponse
     */
    public function dislikeComment(string $commentId): JsonResponse
    {
        $message = $this->likeService->dislikeComment($commentId, Auth::user()->id);

        return response()->json(['message' => $message]);
    }
}
