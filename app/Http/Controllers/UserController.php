<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Collections\UserCollection;
use App\Http\Requests\ManageUser;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;

/**
 * Class LikeController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param string $page
     * @return JsonResponse
     */
    public function index(string $page): JsonResponse
    {
        return response()->json([
            "users" => new UserCollection($this->userService->index($page)),
            "page" => $page,
            "totalPages" => $this->userService->getPagesCount(),
        ]);
    }

    /**
     * @param ManageUser $userId
     * @return JsonResponse
     */
    public function ban(ManageUser $userId): JsonResponse
    {
        $this->userService->ban($userId);

        return response()->json(["Zablokowano użytkownika"]);
    }

    /**
     * @param ManageUser $userId
     * @return JsonResponse
     */
    public function unBan(ManageUser $userId): JsonResponse
    {
        $this->userService->unBan($userId);

        return response()->json(["Odblokowano użytkownika"]);
    }
}
