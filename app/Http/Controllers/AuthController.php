<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateUser;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /** @var AuthService */
    private $authService;

    /**
     * AuthController constructor.
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'activateAccount']]);
        $this->authService = $authService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Wrong credentials'], 401);
        } elseif (Auth::user()->is_banned) {
            return response()->json(['message' => 'Account is banned'], 401);
        } elseif (Auth::user()->activation && !Auth::user()->activation->activated) {
            return response()->json(['message' => 'Account not activated'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @param CreateUser $request
     * @return JsonResponse
     */
    public function register(CreateUser $request)
    {
        $this->authService->createUser($request->all());

        return response()->json(['message' => 'Successfully registered an account']);
    }

    /**
     * @param string $token
     * @return JsonResponse
     */
    public function activateAccount(string $token)
    {
        $this->authService->activateAccount($token);

        return response()->json(['message' => 'Successfully activated the account']);
    }

    /**
     * @return JsonResponse
     */
    public function me()
    {
        return response()->json(Auth::user());
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        Auth::logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::refresh());
    }

    /**
     * @param string $token
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ]);
    }
}
