<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Collections\FlaggedCommentCollection;
use App\Http\Collections\FlaggedEntryCollection;
use App\Http\Requests\ManageReport;
use App\Services\FlaggedCommentService;
use App\Services\FlaggedEntryService;
use App\Services\FlagService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class FlaggedCommentController
 * @package App\Http\Controllers
 */
class FlaggedCommentController extends Controller
{
    /**
     * @var FlaggedCommentService
     */
    private $commentService;
    /**
     * @var FlagService
     */
    private $flagService;

    /**
     * @param FlaggedCommentService $commentService
     * @param FlagService $flagService
     */
    public function __construct(FlaggedCommentService $commentService, FlagService $flagService)
    {
        $this->commentService = $commentService;
        $this->flagService = $flagService;
    }

    /**
     * @param string $commentId
     * @return JsonResponse
     */
    public function flagComment(string $commentId): JsonResponse
    {
        $this->flagService->flagComment($commentId, Auth::user()->id);

        return response()->json(['message' => "Successfully reported the comment"]);
    }

    /**
     * @param string $page
     * @return JsonResponse
     */
    public function index(string $page): JsonResponse
    {
        return response()->json([
            "comments" => new FlaggedCommentCollection($this->commentService->indexFlagged($page)),
            "page" => $page,
            "totalPages" => $this->commentService->getPagesCount(),
        ]);
    }

    /**
     * @param ManageReport $flaggedEntryId
     * @return JsonResponse
     */
    public function dismiss(ManageReport $flaggedEntryId): JsonResponse
    {
        $this->commentService->dismiss($flaggedEntryId);

        return response()->json(['message' => 'Successfully dismissed the report']);
    }
}
