<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateEntryPicture;
use App\Services\EntryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class PictureController
 * @package App\Http\Controllers
 */
class PictureController extends Controller
{
    /**
     * @var EntryService
     */
    private $entryService;

    /**
     * @param EntryService $entryService
     */
    public function __construct(EntryService $entryService)
    {
        $this->entryService = $entryService;
    }

    /**
     * @param CreateEntryPicture $request
     * @return JsonResponse
     */
    public function validatePicture(CreateEntryPicture $request): JsonResponse
    {
        return response()->json(['message' => 'Picture is correct']);
    }

    /**
     * @param string $fileName
     * @return BinaryFileResponse
     */
    public function getImageUrl(string $fileName): BinaryFileResponse
    {
        return $this->getImageResponse($this->entryService->getImageUrl($fileName));
    }

    /**
     * @param int $entryId
     * @param CreateEntryPicture $request
     * @return JsonResponse
     */
    public function createPicture(int $entryId, CreateEntryPicture $request): JsonResponse
    {
        $userId = Auth::user()->id;
        $this->entryService->createEntryPicture($request->picture, $entryId, $userId);

        return response()->json(['message' => 'Successfully added an entry']);
    }

    /**
     * @param string $file_path
     * @return BinaryFileResponse
     */
    protected function getImageResponse(string $file_path): BinaryFileResponse
    {
        return new BinaryFileResponse($file_path, JSONResponse::HTTP_OK, ['Content-Type' => 'image/png']);
    }
}
