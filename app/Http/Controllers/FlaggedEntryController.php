<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Collections\FlaggedEntryCollection;
use App\Http\Requests\ManageReport;
use App\Services\FlaggedEntryService;
use App\Services\FlagService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class FlaggedEntryController
 * @package App\Http\Controllers
 */
class FlaggedEntryController extends Controller
{
    /**
     * @var FlaggedEntryService
     */
    private $entryService;
    /**
     * @var FlagService
     */
    private $flagService;

    /**
     * @param FlaggedEntryService $entryService
     * @param FlagService $flagService
     */
    public function __construct(FlaggedEntryService $entryService, FlagService $flagService)
    {
        $this->entryService = $entryService;
        $this->flagService = $flagService;
    }

    /**
     * @param string $entryId
     * @return JsonResponse
     */
    public function flagEntry(string $entryId): JsonResponse
    {
        $this->flagService->flagEntry($entryId, Auth::user()->id);

        return response()->json(['message' => "Successfully reported the entry"]);
    }

    /**
     * @param string $page
     * @return JsonResponse
     */
    public function index(string $page): JsonResponse
    {
        return response()->json([
            "entries" => new FlaggedEntryCollection($this->entryService->indexFlagged($page)),
            "page" => $page,
            "totalPages" => $this->entryService->getPagesCount(),
        ]);
    }

    /**
     * @param ManageReport $flaggedEntryId
     * @return JsonResponse
     */
    public function dismiss(ManageReport $flaggedEntryId): JsonResponse
    {
        $this->entryService->dismiss($flaggedEntryId);

        return response()->json(['message' => 'Successfully dismissed the report']);
    }

    /**
     * @param ManageReport $flaggedEntryId
     * @return JsonResponse
     */
    public function report(ManageReport $flaggedEntryId): JsonResponse
    {
        $this->entryService->report($flaggedEntryId);

        return response()->json(['message' => 'Successfully reported the report']);
    }
}
