<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Collections\CommentCollection;
use App\Http\Requests\CreateComment;
use App\Http\Requests\ManageComment;
use App\Services\CommentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class CommentController
 * @package App\Http\Controllers
 */
class CommentController extends Controller
{
    /** @var CommentService */
    private $commentService;

    /**
     * CommentController constructor.
     * @param CommentService $commentService
     */
    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    /**
     * @param string $entryId
     * @param string $page
     * @return CommentCollection
     */
    public function list(string $entryId, string $page): CommentCollection
    {
        return new CommentCollection($this->commentService->list($entryId, $page));
    }

    /**
     * @param CreateComment $request
     * @param string $entryId
     * @return JsonResponse
     */
    public function create(CreateComment $request, string $entryId): JsonResponse
    {
        $userId = Auth::user()->id;
        $this->commentService->createComment($request->all(), $userId, $entryId);

        return response()->json(['message' => 'Successfully added a comment']);
    }

    /**
     * @param string $commentId
     * @return JsonResponse
     */
    public function ownerDelete(string $commentId): JsonResponse
    {
        $userId = Auth::user()->id;
        $this->commentService->ownerDeleteComment($commentId, $userId);

        return response()->json(['message' => 'Successfully deleted the comment']);
    }

    /**
     * @param ManageComment $commentId
     * @return JsonResponse
     */
    public function adminDelete(ManageComment $commentId): JsonResponse
    {
        $this->commentService->adminDeleteComment($commentId);

        return response()->json(['message' => 'Successfully deleted the comment']);
    }
}
