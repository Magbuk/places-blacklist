<?php

declare(strict_types=1);

namespace App\Http\Collections;

use App\Http\Resources\FlaggedEntryResource;
use App\Models\FlaggedEntry;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class FlaggedEntryCollection
 * @package App\Http\Collections
 */
class FlaggedEntryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (FlaggedEntry $entry) {
            return (new FlaggedEntryResource($entry));
        });

        return parent::toArray($request);
    }
}
