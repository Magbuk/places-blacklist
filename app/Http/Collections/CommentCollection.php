<?php

declare(strict_types=1);

namespace App\Http\Collections;

use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class CommentCollection
 * @package App\Http\Collections
 */
class CommentCollection extends ResourceCollection
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $this->collection->transform(function (Comment $comment) {
            return (new CommentResource($comment));
        });

        return parent::toArray($request);
    }
}
