<?php

declare(strict_types=1);

namespace App\Http\Collections;

use App\Http\Resources\FlaggedCommentResource;
use App\Models\FlaggedComment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class FlaggedCommentCollection
 * @package App\Http\Collections
 */
class FlaggedCommentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (FlaggedComment $entry) {
            return (new FlaggedCommentResource($entry));
        });

        return parent::toArray($request);
    }
}
