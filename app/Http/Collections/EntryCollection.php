<?php

declare(strict_types=1);

namespace App\Http\Collections;

use App\Http\Resources\EntryResource;
use App\Http\Resources\UserResource;
use App\Models\Entry;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class EntryCollection
 * @package App\Http\Collections
 */
class EntryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Entry $entry) {
            return (new EntryResource($entry));
        });

        return parent::toArray($request);
    }
}
