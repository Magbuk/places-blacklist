<?php

declare(strict_types=1);

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /** @var array */
    protected $listen = [
        'App\Events\ExampleEvent' => [
            'App\Listeners\EntryObserver',
        ],
    ];
}
