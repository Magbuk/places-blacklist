<?php

declare(strict_types=1);

namespace App\Providers;

use App\Models\Comment;
use App\Models\Entry;
use App\Models\User;
use App\Models\UserActivation;
use App\Observers\CommentObserver;
use App\Observers\EntryObserver;
use App\Observers\UserActivationObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

/**
 * Class ObserverServiceProvider
 * @package App\Providers
 */
class ObserverServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Entry::observe(EntryObserver::class);
        Comment::observe(CommentObserver::class);
        User::observe(UserObserver::class);
        UserActivation::observe(UserActivationObserver::class);
    }

    public function register(): void
    {
    }
}
