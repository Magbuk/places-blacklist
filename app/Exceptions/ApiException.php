<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use App\Interfaces\ApiExceptionInterface;
use Throwable;

/**
 * Class ApiException
 * @package ITMhub\Exceptions
 */
class ApiException extends Exception implements ApiExceptionInterface
{
    /**
     * @var array
     */
    protected $messages = [];
    /**
     * @var int
     */
    protected $code = 500;

    /**
     * ApiException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->pushMessage($message);
        $this->setStatusCode($code);
    }

    /**
     * @param string $message
     * @return ApiExceptionInterface
     */
    public function pushMessage(string $message): ApiExceptionInterface
    {
        $this->messages[] = __($message);
        return $this;
    }

    /**
     * @param array $messages
     * @return ApiExceptionInterface
     */
    public function setMessages(array $messages): ApiExceptionInterface
    {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @param int $statusCode
     * @return ApiExceptionInterface
     */
    public function setStatusCode(int $statusCode): ApiExceptionInterface
    {
        $this->code = $statusCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->code;
    }
}
