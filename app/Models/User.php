<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class UserResource
 * @package App\Models\UserResource
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    /* @var array */
    protected $fillable = [
        'name', 'email',
    ];

    /* @var array */
    protected $hidden = [
        'password',
    ];

    /* @var array */
    protected $with = [
        'userRole',
    ];

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * @return HasOne
     */
    public function userRole(): HasOne
    {
        return $this->hasOne(UserRole::class, "user_id");
    }

    /**
     * @return HasOne
     */
    public function activation(): HasOne
    {
        return $this->hasOne(UserActivation::class, "user_id");
    }

    /**
     * @return UserRole
     */
    public function role(): UserRole
    {
        return $this->userRole;
    }

    /**
     * @return HasMany
     */
    public function entryLikes(): HasMany
    {
        return $this->hasMany(EntryLike::class, "entry_id");
    }
}
