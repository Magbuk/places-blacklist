<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserCommentFlag
 * @package App\Models
 */
class UserCommentFlag extends Model
{
}
