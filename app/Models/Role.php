<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Role
 * @package App\Models
 */
class Role extends Model
{
    const SUPERADMIN = 1;
    const ADMIN = 2;
    const USER = 3;

    /**
     * @return BelongsToMany
     */
    public function userRoles(): BelongsToMany
    {
        return $this->belongsToMany(UserRole::class);
    }
}
