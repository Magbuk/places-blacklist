<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class EntryCategory
 * @package App\Models
 */
class EntryCategory extends Model
{
    /* @var array */
    protected $fillable = [
        'name',
    ];

    /**
     * @return HasMany
     */
    public function entries(): HasMany
    {
        return $this->hasMany(Entry::class, "entry_id");
    }
}
