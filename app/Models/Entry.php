<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Entry
 * @package App\Models
 */
class Entry extends Model
{
    /* @var array */
    protected $fillable = [
        'title', 'picture', 'description', 'address', 'category_id', 'visit_date'
    ];

    /** @var array */
    protected $with = [
        'user', 'entryCategory'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id");
    }

    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return BelongsTo
     */
    public function entryCategory(): BelongsTo
    {
        return $this->belongsTo(EntryCategory::class, "category_id");
    }

    /**
     * @return HasMany
     */
    public function likes(): HasMany
    {
        return $this->hasMany(EntryLike::class, "entry_id");
    }

    /**
     * @return HasMany
     */
    public function dislikes(): HasMany
    {
        return $this->hasMany(EntryDislike::class, "entry_id");
    }
}
