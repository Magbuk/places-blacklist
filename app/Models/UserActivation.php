<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserActivation
 * @package App\Models
 */
class UserActivation extends Model
{
    /* @var array */
    protected $fillable = [
        'token', 'activated'
    ];
}
