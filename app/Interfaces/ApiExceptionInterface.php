<?php

declare(strict_types=1);

namespace App\Interfaces;

/**
 * Interface ApiExceptionInterface
 * @package ITMhub\Interfaces
 */
interface ApiExceptionInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @return array
     */
    public function getMessages(): array;

    /**
     * @param array $messages
     * @return ApiExceptionInterface
     */
    public function setMessages(array $messages): ApiExceptionInterface;

    /**
     * @param string $message
     * @return ApiExceptionInterface
     */
    public function pushMessage(string $message): ApiExceptionInterface;

    /**
     * @param int $statusCode
     * @return ApiExceptionInterface
     */
    public function setStatusCode(int $statusCode): ApiExceptionInterface;
}
